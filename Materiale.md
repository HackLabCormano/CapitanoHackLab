# Inventario materiali (e contribuzione)

## Hacklab

* breadboard
* accelerometro
* bussola
* GPS
* ledstrip

## Leonardo

* polistirene
* alloggiamenti 18650

## Marco

* squadrette montaggio

## Atrent

* ESP32
* ventole (regalo)
* scatola (regalo)
* minuterie (regalo)
* connettore IP68 (regalo)