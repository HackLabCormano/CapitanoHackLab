/**
 * COMMENTI:
 * Questi define sono utili per la costruzione del hardware
 * Definiscono quali pin sono occupati da quali componenti
 * Nel Codice sono presenti dei Tag 
 * < // TODO: "Text" > indicano aree di codice che necessitano di attenzione e di sviluppo
 * < // TASK: "Nome" > indicano porzioni di codice che svolgono determinate funzioni
 *                     Questi tag aiutano ad orientarsi nella documentazione del codice
 * < // BUG: "Text" > indicano aree di codice che presentano problemi
 * < COMMENTI > sono aree di codice che presentano una documentazione ridondante nel codice
 * 
 * Sono Configurati i seguenti TASK:
 * TASK: GPS_UPDATE
 * TASK: GYRO_UPDATE
 * TASK: MAG_UPDATE
 * TASK: REPORT (su UART)
 */


// TODO: i motori dovrebbero partire da 0 e non da 1
// TODO: aggiungere i pin del channel 0 uart per la comunicazione con il pc

#define PIN_MOTOR1 13
#define PIN_MOTOR2 12
#define PIN_MOTOR3 14
#define PIN_MOTOR4 27
#define PIN_MOTOR5 26
#define PIN_MOTOR6 25

#define SCA 21
#define SCL 22

#define TX2 17
#define RX2 16


/**
 * COMMENTI:
 *
 * Rappresento il natante come una matrice 3x2 suddividendo i 6 motori in 3 gruppi di 2 motori ciascuno.
 *
 *              | Sinistra  | Destra    |
 *      ________|___________|___________|
 *      Prua    | motor_1   | motor_4   |
 *      ________|___________|___________|
 *      Sterzo  | motor_2   | motor_5   |
 *      ________|___________|___________|
 *      Poppa   | motor_3   | motor_6   |
 *      ________|___________|___________|
 *
 * L'accesso alla matrice avviene come :
 *
 *  natante[<Riga>][<Colonna>]
 *
 *  DOMINIO DI RIGA:
 *  - PRUA
 *  - STERZO
 *  - POPPA
 *
 *  DOMINIO DI COLONNA
 *  - SX
 *  - DX
 */
#define NATANTE_N_ROWS 3
#define NATANTE_N_COLS 2

#define PRUA 0
#define STERZO 1
#define POPPA 2
#define SX 0
#define DX 1

const int natante[3][2] =
    {
        {PIN_MOTOR1, PIN_MOTOR4},
        {PIN_MOTOR2, PIN_MOTOR5},
        {PIN_MOTOR3, PIN_MOTOR6}
    };

/**
 * COMMENTI:
 * Questa sezione del codice è dedicata alla gestione dei sensori
 * Attualmente sono presenti 3 sensori:
 * - GPS : Neo 6M
 * - Giroscopio a tre assi: MPU6050
 * - Bussola: HMC5883
 * 
 * I sensori sono inizializzati e configurati all'interno della funzione setup()
 * I dati dei sensori sono letti dai rispettivi TASK
 * i protocolli in uso sono:
 * - UART:  per la comunicazione con il GPS
 * - I2C:   per la comunicazione con il giroscopio e la bussola
 *          viene sfruttato un bus comune per entrambi i sensori avendo indirizzi univoci
 *          - MPU6050: 0x68
 *          - HMC5883: 0x1E
 * 
*/

#include <HardwareSerial.h>
#include <TinyGPSPlus.h>
#define GPS_SERIAL_BAUNDRATE 9600
#define SERIAL_CHANNEL 2

TinyGPSPlus gps;
HardwareSerial serialGPS(SERIAL_CHANNEL);


#include <Wire.h>

#include <MPU6050_tockn.h>

MPU6050 mpu6050(Wire);

/**
 * COMMENTI:
 * L'azimuth è la distanza angolare tra il nord magnetico e la direzione in cui si sta guardando
 * Nel Codice è calcolato come valore in gradi sempre positivo compreso tra 0 e 360, dove 0 è il nord magnetico
 */

#include <Adafruit_Sensor.h>
#include <Adafruit_HMC5883_U.h>

Adafruit_HMC5883_Unified mag = Adafruit_HMC5883_Unified(12345);
sensor_t sensor;
sensors_event_t event;
float azimuth;

#define XCORRECTION 1
#define YCORRECTION 20

#include <TaskScheduler.h>

/**
 * COMMENTI:
 * i task devono occuparsi di inizializzare le proprie risorse all'avvio nella funzione bool onEnable()
 * se l'inizializzazione non ha successo il task deve impedire il proprio avvio
 *
 */

Scheduler task_queue;
#define SCHEDULER &task_queue
#define SCHEDULER_TASK_OFF  false
#define SCHEDULER_TASK_ON   true

#define GPS_TIME 200
void GPS_EXEC();
bool GPS_ENABLE();
void GPS_DISABLE();
Task GPS_UPDATE (GPS_TIME, TASK_FOREVER, &GPS_EXEC, SCHEDULER, SCHEDULER_TASK_OFF ,&GPS_ENABLE , &GPS_DISABLE);
void GPS_EXEC(){
  while(serialGPS.available() > 0){
    gps.encode(serialGPS.read());
  }
}

bool GPS_ENABLE(){
  serialGPS.begin(GPS_SERIAL_BAUNDRATE, SERIAL_8N1, RX2, TX2);
  return true;
}

void GPS_DISABLE(){
  serialGPS.end();
}

#define GYRO_TIME 100
void GYRO_EXEC();
bool GYRO_ENABLE();
void GYRO_DISABLE();
Task GYRO_UPDATE (GYRO_TIME, TASK_FOREVER, &GYRO_EXEC, SCHEDULER, SCHEDULER_TASK_OFF, &GYRO_ENABLE, &GYRO_DISABLE);
void GYRO_EXEC(){
  mpu6050.update();
}

bool checkMPU6050() {
  Wire.beginTransmission(0x68); // Indirizzo I2C dell'MPU6050
  byte error = Wire.endTransmission();
  
  if (error == 0) {
    return true;  // Dispositivo trovato e risponde
  } else {
    return false; // Dispositivo non risponde o non è connesso
  }
}

bool GYRO_ENABLE(){
  mpu6050.begin();
  mpu6050.calcGyroOffsets();
  return checkMPU6050();
}

void GYRO_DISABLE(){
}

#define MAG_TIME 200
void MAG_EXEC();
bool MAG_ENABLE();
void MAG_DISABLE();
Task MAG_UPDATE (MAG_TIME, TASK_FOREVER, &MAG_EXEC, SCHEDULER, SCHEDULER_TASK_OFF, &MAG_ENABLE, &MAG_DISABLE);
void MAG_EXEC(){
  mag.getSensor(&sensor);
  mag.getEvent(&event);

  float heading = atan2(event.magnetic.y + YCORRECTION, event.magnetic.x + XCORRECTION);

  float declinationAngle = 0.04;
  heading += declinationAngle;

  if (heading < 0)
    heading += 2 * PI;

  if (heading > 2 * PI)
    heading -= 2 * PI;

  azimuth = heading * 180 / M_PI;
}

bool MAG_ENABLE(){
  if (!mag.begin()) {
    Serial.println("HMC5883 Not Found");
    return false;
  }
  return true;
}

void MAG_DISABLE(){
}


#define REPORT_TIMER 1000
void SERIAL_REPORT();
Task REPORT(REPORT_TIMER, TASK_FOREVER, &SERIAL_REPORT, SCHEDULER, SCHEDULER_TASK_OFF);
void SERIAL_REPORT(){
  // TASK: PRINT DATA
  Serial.println(F("===== REPORT GPS ====="));
  if (gps.location.isValid()) {
      Serial.print(F("Latitudine: "));
      Serial.print(gps.location.lat(), 6); // 6 cifre decimali per massima precisione
      Serial.print(F(" | Longitudine: "));
      Serial.print(gps.location.lng(), 6); // 6 cifre decimali per massima precisione

      Serial.print(F(" | HDOP (precisione orizzontale): "));
      Serial.println(gps.hdop.value() / 100.0); // HDOP, minore è meglio (0.5-2.0 = ottimo)
    }

    // Controlla se l'altitudine è aggiornata
    if (gps.altitude.isValid()) {
      Serial.print(F("Altitudine: "));
      Serial.print(gps.altitude.meters(), 2); // Precisione di 2 cifre decimali
      Serial.println(F(" metri"));
    }

    // Controlla il numero di satelliti
    Serial.print(F("Satelliti visibili: "));
    Serial.println(gps.satellites.value());


  Serial.println(F("===== REPORT MPU6050 ====="));

  // Accelerazione (valori in g)
  Serial.print(F("Accelerazione X: "));
  Serial.print(mpu6050.getAccX(), 3); // 3 cifre decimali
  Serial.print(F(" g | Accelerazione Y: "));
  Serial.print(mpu6050.getAccY(), 3);
  Serial.print(F(" g | Accelerazione Z: "));
  Serial.println(mpu6050.getAccZ(), 3);

  // Velocità angolare (in gradi/s)
  Serial.print(F("Velocità Angolare X: "));
  Serial.print(mpu6050.getGyroX(), 3);
  Serial.print(F(" °/s | Velocità Angolare Y: "));
  Serial.print(mpu6050.getGyroY(), 3);
  Serial.print(F(" °/s | Velocità Angolare Z: "));
  Serial.println(mpu6050.getGyroZ(), 3);

  // Angoli calcolati (Roll, Pitch, Yaw)
  Serial.print(F("Angolo Roll: "));
  Serial.print(mpu6050.getAngleX(), 2); // 2 cifre decimali
  Serial.print(F(" ° | Angolo Pitch: "));
  Serial.print(mpu6050.getAngleY(), 2);
  Serial.print(F(" ° | Angolo Yaw: "));
  Serial.println(mpu6050.getAngleZ(), 2);

  // Temperatura
  Serial.print(F("Temperatura: "));
  Serial.print(mpu6050.getTemp(), 2); // 2 cifre decimali
  Serial.println(F(" °C"));

  Serial.println(F("===== REPORT MAG ====="));

  Serial.print(F("AZIMUTH: "));
  Serial.print(azimuth);
  Serial.println(F(" °"));
}

/**
 * COMMENTI:
 * SetUp 
 * inizializza UART per il debug.
 * Avvia i task e stampa un info di debug se il processo viene avviato.
 */
void setup(){

  Serial.begin(115200);
  Wire.begin(SDA, SCL); 

  // GPS

  if(GPS_UPDATE.enable()){
    Serial.println("GPS -> Enable");
  }

  if(GYRO_UPDATE.enable()){
    Serial.println("GYRO -> Enable");
  }

  if(MAG_UPDATE.enable()){
    Serial.println("MAG -> Enable");
  }

  REPORT.enable();
  Serial.println("UART REPORT -> Enable");
  

  Serial.println("MOTOR");
  // NATANTE MOTORI
  for (int i = 0; i < NATANTE_N_ROWS; i++)  {
    for (int ii = 0; ii < NATANTE_N_COLS; ii++) {
      pinMode(natante[i][ii], OUTPUT);
    }
  }

  Serial.println("END SETUP");
}


void loop(){
  task_queue.execute();
}
